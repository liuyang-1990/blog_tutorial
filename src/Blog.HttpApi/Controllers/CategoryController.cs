﻿using System.Threading.Tasks;
using Blog.Business;
using Blog.CategoryDto;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Blog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : AbpController
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateCategoryDto input)
        {
            await _categoryService.CreateAsync(input);
            return Ok();
        }
    }
}