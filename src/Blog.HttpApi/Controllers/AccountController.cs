﻿using Blog.Business;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Blog.AccountDto;
using Volo.Abp.AspNetCore.Mvc;

namespace Blog.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : AbpController
    {
        private readonly IAuthorizeService _authorizeService;

        public AccountController(IAuthorizeService authorizeService)
        {
            _authorizeService = authorizeService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto dto)
        {
            var token = await _authorizeService.GetAccessToken(dto);
            return Ok(token);

        }

    }
}