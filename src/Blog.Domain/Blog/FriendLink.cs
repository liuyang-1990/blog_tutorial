﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class FriendLink : AuditedEntity<Guid>
    {
        public string LinkName { get; set; }

        public string LinkUrl { get; set; }

    }
}