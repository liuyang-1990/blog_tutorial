﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class UserInfo : AuditedEntity<Guid>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Avatar { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}