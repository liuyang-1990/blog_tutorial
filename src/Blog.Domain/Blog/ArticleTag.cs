﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class ArticleTag : AuditedEntity<Guid>
    {
        public ArticleInfo ArticleInfo { get; set; }
        public Guid ArticleId { get; set; }
        public Tag Tag { get; set; }

        public Guid TagId { get; set; }
    }
}