﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class Category : AuditedEntity<Guid>
    {
        public string CategoryName { get; set; }

        public string Description { get; set; }
        public ArticleCategory ArticleCategory { get; set; }

    }
}