﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class ArticleCategory : AuditedEntity<Guid>
    {
        public ArticleInfo ArticleInfo { get; set; }
        public Guid ArticleId { get; set; }
        public Category Category { get; set; }

        public Guid CategoryId { get; set; }
    }
}