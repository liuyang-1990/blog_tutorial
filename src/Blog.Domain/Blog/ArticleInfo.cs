﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    /// <summary>
    /// 文章表
    /// </summary>
    public class ArticleInfo : AuditedEntity<Guid>
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string Abstract { get; set; }

        /// <summary>
        /// 文章状态 1 发布 0 草稿
        /// </summary>
        public bool IsPublished { get; set; }

        /// <summary>
        /// 摘要右侧图片
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public bool IsTop { get; set; }

        /// <summary>
        /// 访问量
        /// </summary>
        public int Views { get; set; }

        /// <summary>
        /// 评论量
        /// </summary>
        public int Comments { get; set; }

        /// <summary>
        /// 点赞量
        /// </summary>
        public int Likes { get; set; }

        public ArticleContent ArticleContent { get; set; }
        public List<ArticleCategory> ArticleCategories { get; set; }
        public List<ArticleTag> ArticleTags { get; set; }

    }
}