﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class ArticleContent : AuditedEntity<Guid>
    {
        public ArticleInfo ArticleInfo { get; set; }
        public Guid ArticleId { get; set; }
        public string Content { get; set; }
    }
}