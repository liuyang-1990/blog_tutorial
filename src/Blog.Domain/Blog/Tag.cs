﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace Blog.Domain.Blog
{
    public class Tag : AuditedEntity<Guid>
    {
        public string TagName { get; set; }

        public string Description { get; set; }

        public ArticleTag ArticleTag { get; set; }

    }
}