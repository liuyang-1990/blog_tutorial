﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "App_ArticleInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Abstract = table.Column<string>(maxLength: 500, nullable: false),
                    IsPublished = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(maxLength: 200, nullable: true),
                    IsTop = table.Column<bool>(nullable: false),
                    Views = table.Column<int>(nullable: false),
                    Comments = table.Column<int>(nullable: false),
                    Likes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_ArticleInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_Category",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    CategoryName = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_FriendLink",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    LinkName = table.Column<string>(maxLength: 20, nullable: false),
                    LinkUrl = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_FriendLink", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    TagName = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_UserInfo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    UserName = table.Column<string>(maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "char(32)", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Avatar = table.Column<string>(maxLength: 200, nullable: true),
                    Phone = table.Column<string>(type: "char(11)", nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_UserInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "App_ArticleContent",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    ArticleId = table.Column<Guid>(nullable: false),
                    Content = table.Column<string>(type: "longtext", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_ArticleContent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_App_ArticleContent_App_ArticleInfo_ArticleId",
                        column: x => x.ArticleId,
                        principalTable: "App_ArticleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "App_ArticleCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    ArticleInfoId = table.Column<Guid>(nullable: true),
                    ArticleId = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_ArticleCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_App_ArticleCategory_App_ArticleInfo_ArticleInfoId",
                        column: x => x.ArticleInfoId,
                        principalTable: "App_ArticleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_App_ArticleCategory_App_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "App_Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "App_ArticleTag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorId = table.Column<Guid>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierId = table.Column<Guid>(nullable: true),
                    ArticleInfoId = table.Column<Guid>(nullable: true),
                    ArticleId = table.Column<Guid>(nullable: false),
                    TagId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App_ArticleTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_App_ArticleTag_App_ArticleInfo_ArticleInfoId",
                        column: x => x.ArticleInfoId,
                        principalTable: "App_ArticleInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_App_ArticleTag_App_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "App_Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_App_ArticleCategory_ArticleInfoId",
                table: "App_ArticleCategory",
                column: "ArticleInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_App_ArticleCategory_CategoryId",
                table: "App_ArticleCategory",
                column: "CategoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_App_ArticleContent_ArticleId",
                table: "App_ArticleContent",
                column: "ArticleId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_App_ArticleTag_ArticleInfoId",
                table: "App_ArticleTag",
                column: "ArticleInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_App_ArticleTag_TagId",
                table: "App_ArticleTag",
                column: "TagId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "App_ArticleCategory");

            migrationBuilder.DropTable(
                name: "App_ArticleContent");

            migrationBuilder.DropTable(
                name: "App_ArticleTag");

            migrationBuilder.DropTable(
                name: "App_FriendLink");

            migrationBuilder.DropTable(
                name: "App_UserInfo");

            migrationBuilder.DropTable(
                name: "App_Category");

            migrationBuilder.DropTable(
                name: "App_ArticleInfo");

            migrationBuilder.DropTable(
                name: "App_Tag");
        }
    }
}
