﻿using Blog.Domain;
using Blog.Domain.Blog;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Blog.EntityFrameworkCore
{
    public static class BlogDbContextModelCreatingExtensions
    {
        public static void ConfigureBlog(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */
            builder.Entity<ArticleInfo>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "ArticleInfo", BlogConsts.DbSchema);
                b.Property(x => x.Title).HasMaxLength(200).IsRequired();
                b.Property(x => x.Abstract).HasMaxLength(500).IsRequired();
                b.Property(x => x.ImageUrl).HasMaxLength(200);
                b.ConfigureByConvention();
            });

            builder.Entity<ArticleInfo>()
                .HasOne(x => x.ArticleContent)
                .WithOne(r => r.ArticleInfo)
                .HasForeignKey<ArticleContent>(r => r.ArticleId);

            builder.Entity<ArticleContent>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "ArticleContent", BlogConsts.DbSchema);
                b.Property(x => x.Content).HasColumnType("longtext").IsRequired();
                b.ConfigureByConvention();
            });

            builder.Entity<ArticleCategory>(b => { b.ConfigureByConvention(); });
            builder.Entity<ArticleCategory>()
                .ToTable(BlogConsts.DbTablePrefix + "ArticleCategory", BlogConsts.DbSchema)
                .HasOne(x => x.ArticleInfo)
                .WithMany(b => b.ArticleCategories);
            builder.Entity<ArticleCategory>()
                .HasOne(x => x.Category)
                .WithOne(b => b.ArticleCategory)
                .HasForeignKey<ArticleCategory>(r => r.CategoryId);

            builder.Entity<ArticleTag>(b => { b.ConfigureByConvention(); });
            builder.Entity<ArticleTag>()
                .ToTable(BlogConsts.DbTablePrefix + "ArticleTag", BlogConsts.DbSchema)
                .HasOne(x => x.ArticleInfo)
                .WithMany(b => b.ArticleTags);
            builder.Entity<ArticleTag>()
                .HasOne(x => x.Tag)
                .WithOne(b => b.ArticleTag)
                .HasForeignKey<ArticleTag>(r => r.TagId);

            builder.Entity<Category>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "Category", BlogConsts.DbSchema);
                b.Property(x => x.CategoryName).HasMaxLength(50).IsRequired();
                b.Property(x => x.Description).HasMaxLength(50);
                b.ConfigureByConvention();
            });

            builder.Entity<Tag>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "Tag", BlogConsts.DbSchema);
                b.Property(x => x.TagName).HasMaxLength(50).IsRequired();
                b.Property(x => x.Description).HasMaxLength(50);
                b.ConfigureByConvention();
            });

            builder.Entity<FriendLink>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "FriendLink", BlogConsts.DbSchema);
                b.Property(x => x.LinkName).HasMaxLength(20).IsRequired();
                b.Property(x => x.LinkUrl).HasMaxLength(100).IsRequired();
                b.ConfigureByConvention();
            });
            builder.Entity<UserInfo>(b =>
            {
                b.ToTable(BlogConsts.DbTablePrefix + "UserInfo", BlogConsts.DbSchema);
                b.Property(x => x.UserName).HasMaxLength(20).IsRequired();
                b.Property(x => x.Password).HasColumnType("char(32)").IsRequired();
                b.Property(x => x.Phone).HasColumnType("char(11)");
                b.Property(x => x.Email).HasMaxLength(100);
                b.Property(x => x.Avatar).HasMaxLength(200);
                b.ConfigureByConvention();
            });
        }
    }
}