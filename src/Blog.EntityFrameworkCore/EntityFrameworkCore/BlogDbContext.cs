﻿using Blog.Domain.Blog;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace Blog.EntityFrameworkCore
{
    /* This is your actual DbContext used on runtime.
     * It includes only your entities.
     * It does not include entities of the used modules, because each module has already
     * its own DbContext class. If you want to share some database tables with the used modules,
     * just create a structure like done for AppUser.
     *
     * Don't use this DbContext for database migrations since it does not contain tables of the
     * used modules (as explained above). See BlogMigrationsDbContext for migrations.
     */
    [ConnectionStringName("Default")]
    public class BlogDbContext : AbpDbContext<BlogDbContext>
    {

        /* Add DbSet properties for your Aggregate Roots / Entities here.
         * Also map them inside BlogDbContextModelCreatingExtensions.ConfigureBlog
         */
        public virtual DbSet<ArticleInfo> ArticleInfos { get; set; }

        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<Tag> Tags { get; set; }

        public virtual DbSet<ArticleCategory> ArticleCategories { get; set; }

        public virtual DbSet<ArticleTag> ArticleTags { get; set; }

        public virtual DbSet<ArticleContent> ArticleContents { get; set; }


        public BlogDbContext(DbContextOptions<BlogDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);



            /* Configure your own tables/entities inside the ConfigureBlog method */

            builder.ConfigureBlog();
        }
    }
}
