﻿using AutoMapper;
using Blog.CategoryDto;
using Blog.Domain.Blog;

namespace Blog
{
    public class BlogApplicationAutoMapperProfile : Profile
    {
        public BlogApplicationAutoMapperProfile()
        {
            CreateMap<Category, CreateCategoryDto>().ReverseMap();
        }
    }
}
