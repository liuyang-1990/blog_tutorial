﻿using Blog.AccountDto;
using Blog.Domain.Blog;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Blog.Business.Implement
{
    public class AuthorizeService : ApplicationService, IAuthorizeService
    {
        private readonly IRepository<UserInfo> _repository;
        private readonly IConfiguration _configuration;
        public AuthorizeService(IRepository<UserInfo> repository, IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }
        public async Task<string> GetAccessToken(LoginDto dto)
        {
            var userInfo = await _repository.FindAsync(x => x.UserName == dto.UserName &&
                                                            x.Password.ToMd5() == dto.Password);
            if (userInfo == null)
            {
                throw new UserFriendlyException("用户名或密码不正确");
            }

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, userInfo.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.Now.ToUnixTimeSeconds().ToString(),
                    ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.Name, userInfo.UserName)
            };
            var now = DateTime.UtcNow;
            var key = new SymmetricSecurityKey(_configuration["Authentication:JwtBearer:SecurityKey"].GetBytes());
            var jwt = new JwtSecurityToken(
                issuer: _configuration["Authentication:JwtBearer:Issuer"],
                _configuration["Authentication:JwtBearer:Audience"],
                claims: claims,
                notBefore: now,
                expires: now.Add(TimeSpan.FromMinutes(int.Parse(_configuration["Authentication:JwtBearer:Expires"]))),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}