﻿using Blog.CategoryDto;
using Blog.Domain.Blog;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Blog.Business.Implement
{
    public class CategoryService : ApplicationService, ICategoryService
    {
        private readonly IRepository<Category> _categoryRepository;
        public CategoryService(IRepository<Category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task CreateAsync(CreateCategoryDto input)
        {
            await _categoryRepository.InsertAsync(ObjectMapper.Map<CreateCategoryDto, Category>(input));
        }
    }
}