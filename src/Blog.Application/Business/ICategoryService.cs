﻿using Blog.CategoryDto;
using System.Threading.Tasks;

namespace Blog.Business
{
    public interface ICategoryService
    {
        Task CreateAsync(CreateCategoryDto input);
    }
}