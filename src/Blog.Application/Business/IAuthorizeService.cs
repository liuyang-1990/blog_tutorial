﻿using System.Threading.Tasks;
using Blog.AccountDto;

namespace Blog.Business
{
    public interface IAuthorizeService
    {
        Task<string> GetAccessToken(LoginDto dto);
    }
}